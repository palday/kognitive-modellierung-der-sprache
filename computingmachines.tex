\documentclass[a4paper,10pt]{scrartcl}

\usepackage{lmodern}
\usepackage[utf8x]{inputenc}
\usepackage[T1]{fontenc}			% enable extra punctuation output 
\usepackage[english]{babel}		% the majority of this document is in German
\usepackage{natbib}				% extra bibliography tools
\usepackage{bibgerm}				% German APA like bibliography
\usepackage{fixltx2e}
\usepackage{booktabs}			% professional looking tables
\usepackage[top=2.5cm, bottom=2.5cm, right=2.5cm, left=2.5cm]{geometry}
\usepackage{epigraph}
\usepackage[parfill]{parskip}
\usepackage{graphicx}
\usepackage{hyperref}
\usepackage{pgfpages}
\usepackage{tikz}
\usetikzlibrary{arrows,automata}

\usepackage{color}

\newcommand{\enquote}[1]{\frqq{}#1\flqq{}}
\newcommand{\incomplete}[1]{{\color{red}#1}}

\title{Computing Machines}
\author{Phillip M Alday}

\date{April 2013}

\begin{document}
\maketitle

\section{Introduction}
\epigraph{The only constructive theory connecting neuroscience and psychology will arise from the study of software.}{Alan J. Perlis}

Cognitive science is a multidisciplinary pursuit involving psychology, neuroscience, computer science, linguistics and even philosophy.  
Broadly, we can think of cognitive science as the attempt to discover the mapping of (input) stimulus to behavior(al output). 
The extent to which we want to describe the actual implementation of the mapping in mental processes, or more fundamentally, in neurobiology or  ``only'' describe it in terms of correlations in input and output forms the core of the behavioralism vs. cognitivism debate \citep{skinner1957a,chomsky1967a}. 
For now, we can leave such metascientific and philosophical things as descriptive adequacy aside and consider such mappings from a more computational perspective \citep[for more on this, see][]{chomsky1965a}.

We can think of the mapping between stimulus and behavior as a form of computation. 
The study of computation lies that grey area between theoretical computer science and applied mathematics. 
In the following, we will discuss various theoretical models of computation, their isomorphy with classes of formal languages  

\section{Computer Science: Theory of Computation}
\epigraph{Computer Science is no more about computers than astronomy is about telescopes.}{Edsger W. Disjktra}
At the beginning of the twentieth century, the ``foundational crisis'' in mathematics was concerned with the very nature of mathematical knowledge and whether such knowledge stood on firm basis.
Several schools of thought emerged, but the efforts to reduce mathematical knowledge down a large collection of proofs following from a minimal set of axioms (assumptions) were ultimately thwarted by Kurt Gödel's Incompleteness Theorem. 
Amongst other things, Gödel's Theorem implies that there are true, yet unprovable propositions in any consistent\footnote{without internal contradiction} non trivial logical system. 

At roughly the same time and spurred on by the increasing importance of cryptography in warfare, several mathematicians were beginning to study the notion of computability. 
Initially, the focus was on developing theoretical tools to use in determining the correctness and feasibility (i.e.\ efficiency or complexity) of certain algorithms. 
Three main approaches emerged in these attempts to develop the tools to reason about algorithms: the lambda calculus, automata theory, and work with formal languages and recursion. 
The crown jewel of this research was the \emph{Entscheidungsproblem} from David Hilbert, which dealt with the feasibility of a universal algorithm for deciding whether a proposition follows from a set of axioms, which all three approaches were successful in solving.
Already, the parallel to Gödel's work should be apparent.

\textbf{Lambda calculus} is a formal system developed by Alonzo Church which uses variable binding and substitution to express computational procedures.  
LISP, traditionally a very popular computer language in artificial intelligence (AI) research, has a syntax that maps cleanly onto the primitives of the lambda calculus. 
An important concept is the notion of composability: we can take the output of one function as the input of another: $f(g(x))$, often written as $f\circ{}g(x)$. 
Furthermore, functions can both take functions as input and yield functions as output. 
An example of this from high school calculus is the derivative $\frac{d}{dx}$.

\textbf{Automata theory} is concerned with the study of abstract computing machines called automata. 
Perhaps the simplest class of automata are the finite-state automata (FSA), an example of which is shown in Fig.\,\ref{fig:fsa}. Note that despite the finite number of states, the FSA is capable of handling arbitrarily large input! Other common automata are stack machines (pushdown automata), which have an additional ``stack'' onto which they can push and remove the topmost item, and the Turing machine (named after its inventor Alan Turing), which can be conceived of a small device rolling up and down a (infinite) track of paper tape \citep[cf.][]{turing1936a}. 
The device reads from the tape and performs a manipulation from a finite list on the value (which manipulation is based on the machine's internal state). 
Based on the result of this manipulation, the machine may alter its internal state, write something back onto the tape and move forward or backward along the tape. 

\begin{figure}
\centering
\begin{tikzpicture}[->,>=stealth',shorten >=1pt,auto,node distance=2.8cm,
                    semithick]
  %\tikzstyle{every state}=[fill=red,draw=none,text=white]

  \node[initial,state] (A)                    {even};
  \node[state]         (B)   [right of=A]     {odd};
  
  \path (A) edge [bend left]   node {$+1$} (B);
  \path (B) edge [bend left]   node {$+1$} (A);
 % \path (A) edge [loop above]   node {$+2$} (B);
\end{tikzpicture}
\caption{Simple finite state machine for determining if the $n$th integer greater than 0 is even or odd. Start at ``even'' with zero and count upwards until you reach $n$, following the state transition arrows as you count. (This example is a tad oversimplified, but suffices for our purposes.)}\label{fig:fsa}
\end{figure}

\begin{figure}
\centering
\includegraphics[width=0.5\textwidth]{turingmachine.png}
\caption{Artist's conception of a Turing machine. Source: \url{https://en.wikipedia.org/wiki/File:Maquina.png}}\label{fig:turing}
\end{figure}

The FSA in Fig.\,\ref{fig:fsa} is deterministic: for a given input, it will always perform exactly the same subcomputations in the same order in the same way. 
Nondeterministic machines are also useful and interesting, although more difficult to study. 
For example, we could consider a non deterministic sorting device for cards that randomly orders them (perhaps it throws all the cards up in the air and collects them as they fall). 
The computation would not be guaranteed to succeed, but when it did, it would perform the computation \emph{very} quickly. 
On the other hand, many common deterministic sorting algorithms have a processing time that is proportion to the square of the number of items to be sorted. 
This means sort gets very slow very quickly!%
\footnote{The theoretical best case for deterministic comparison-based sort, i.e. sorting as we know it, is $O(n \log n)$, which a lot better than the $O(n^2)$ in the previous example, but still not quite as quick as when the non deterministic sort gets lucky.}
 
\textbf{Recursion} describes a structure containing an example of itself or, equivalently, defined  in terms of itself. 
Recursion turns out to be a surprisingly powerful and important tool in most fields of study dealing with computation.
Giuseppe Peano used recursion to construct the natural numbers, with which we can define the integers, then the rational numbers, and so forth.
Stephen Kleene with Alonzo Church worked on recursion to develop the theory of formal languages. 
Many formulations about the computational complexity of language are stated in the vocabulary of formal languages, so \textbf{beware}: many words have a different, much more specific meaning in this context, including such simple words as \emph{language} or \emph{word}.
Moreover, formal grammars are completely ignorant of concepts such as ``semantics'' which means that manipulating toy grammars which use words from a natural language as their symbol set can very quickly lead to false intuitions and assumptions if you're not careful! 

\incomplete{
It turns out that all three approaches are more or less equivalent. 
Indeed, there is a direct isomorphism between the classes of formal grammars and types of automata. 
The most basic or broad relationship between different classes of grammar, or equivalently, of automata are given in the Chomsky(-Schützenberger) Hierarchy.

this part will be expanded later. go through Chomsky hierarchy, explaining the classes of languages along the way.

Fun with computability do example with Busy Beavers 

Entscheidungsproblem is a special case of the halting problem
Turing completeness, computability
}

\section{Computation in Practice: the Von Neumann Architechture}
\epigraph{The question of whether Machines Can Think... is about as relevant as the question of whether Submarines Can Swim.}{Edsger W. Disjktra}

\incomplete{
These theoretical tools provide the means for us to reason about computation, but do not reflect the actual way by which computers compute.

idealized register machine as post Turing machine

modern computers are register machines, but the JVM is a stack machine!

von Neumann architecture with tikz graphic

side bar on parallel / serial and GPGPU, if there's enough time

Reduction of everything to binary, on-off. different type of arithmetic (XOR and whatnot), perhaps logic gates if there's time. ``reality'' of software concepts like object in the hardware (priming the comparison the neurobiological reality of mental processes and computational units)
}



\bibliographystyle{gerapali}
\tiny
\bibliography{$HOME/Dropbox/alday.bib}
\end{document}